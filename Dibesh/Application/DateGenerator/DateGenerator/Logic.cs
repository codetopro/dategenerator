﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DateGenerator
{
    class Logic
    {
        string output;//To store output
        string Normal(string number)
        {
            char check;//To check condition
            int count = number.Length+ 1;//Lenth of digit +1
            char[] member = new char[count];//Charecter array of the count
            member = number.ToCharArray();//Copying string to charecter array
            char[] C = new char[count];// Char array to store output of C
            char[] A = new char[count];// Char array to store output of A
            char[] B = new char[count];// Char array to store output of B
            switch (count)
            {
                case 9://9 means 8 
                    B[0] = member[0];//1st character
                    B[1] = member[2];//3rd  Character
                    B[2] = member[4];//5th Character
                    B[3] = member[5];//6th Character
                    B[4] = member[6];//7th Character
                    B[5] = member[7];//8th Character
                    output = new string(B);
                    break;
                case 10://10 means 9
                    check = member[2];
                    if (check == '/')
                    {
                        C[0] = member[0];//1st character
                        C[1]= member[1];//2nd  Character
                        C[2] = member[3];//4th Character
                        C[3] = member[5];//6th Character
                        C[4] = member[6];//7th Character
                        C[5] = member[7];//8th Character
                        C[6] = member[8];//9th Character
                        output = new string(C);
                    }
                    else
                    {
                        C[0] = member[0];//1st character
                        C[1] = member[2];//3rd  Character
                        C[2] = member[3];//4th Character
                        C[3] = member[5];//6th Character
                        C[4] = member[6];//7th Character
                        C[5] = member[7];//8th Character
                        C[6] = member[8];//9th Character
                        output = new string(C);
                    }
                    break;
                case 11://11 means 10
                    A[0] = member[0];//1st character
                    A[1] = member[1];//2nd  Character
                    A[2] = member[3];//4th Character
                    A[3] = member[4];//5th Character
                    A[4] = member[6];//7th Character
                    A[5] = member[7];//8th Character
                    A[6] = member[8];//9th Character
                    A[7] = member[9];//10th Character
                    output = new string(A);
                    break;
                default:
                    MessageBox.Show("You made a bo bo");
                    break;
            }
            return output;
        }
        public  string EncodeNormal(string Number)
        {
            return Normal(Number);
        }

        string Advance(string number)
        {
            char check;//To check condition
            int count = number.Length + 1;//Lenth of digit +1
            char[] member = new char[count];//Charecter array of the count
            member = number.ToCharArray();//Copying string to charecter array
            char[] C = new char[count];// Char array to store output of C
            char[] A = new char[count];// Char array to store output of A
            char[] B = new char[count];// Char array to store output of B
            switch (count)
            {
                case 9://9 means 8 
                    B[0] = member[4];//5th Character    Year
                    B[1] = member[5];//6th Character    Year
                    B[2] = member[6];//7th Character    Year
                    B[3] = member[7];//8th Character    Year
                    B[4] = member[0];//1st character    Month
                    B[5] = member[2];//3rd  Character   Day
                    output = new string(B);
                    break;
                case 10://10 means 9
                    check = member[2];
                    if (check == '/')
                    {
                        C[0] = member[5];//6th Character    Year
                        C[1] = member[6];//7th Character    Year
                        C[2] = member[7];//8th Character    Year
                        C[3] = member[8];//9th Character    Year
                        C[4] = member[0];//1st character
                        C[5] = member[1];//2nd  Character
                        C[6] = member[3];//4th Character
                        output = new string(C);
                    }
                    else
                    {
                        C[0] = member[5];//6th Character    Year
                        C[1] = member[6];//7th Character    Year
                        C[2] = member[7];//8th Character    Year
                        C[3] = member[8];//9th Character    Year
                        C[4] = member[0];//1st character
                        C[5] = member[2];//3rd  Character
                        C[6] = member[3];//4th Character
                        output = new string(C);
                    }
                    break;
                case 11://11 means 10
                    A[0] = member[6];//7th Character    Year
                    A[1] = member[7];//8th Character    Year 
                    A[2] = member[8];//9th Character    Year
                    A[3] = member[9];//10th Character   Year
                    A[4] = member[0];//1st character    Month
                    A[5] = member[1];//2nd  Character   Month
                    A[6] = member[3];//4th Character    Day
                    A[7] = member[4];//5th Character    Day
                    output = new string(A);
                    break;
                default:
                    MessageBox.Show("You made a bo bo");
                    break;
            }
            return output;
        }
        public string EncodeAdvance(string Number)
        {
            return Advance(Number);
        }
    }
}
