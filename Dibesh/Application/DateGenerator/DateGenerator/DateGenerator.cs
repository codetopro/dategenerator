﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DateGenerator
{
    public partial class DateGenerator : Form
    {
        string todaysDate;
        Logic logic = new Logic();
        public DateGenerator()
        {
            InitializeComponent();
            todaysDate = DateTime.Now.ToShortDateString();
            labelOutputNormal.Text = logic.EncodeNormal(todaysDate);
            labelOutputAdvance.Text = logic.EncodeAdvance(todaysDate);
        }

        private void DateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            labelOutputNormal.Text = logic.EncodeNormal(dateTimePicker.Text);
            labelOutputAdvance.Text = logic.EncodeAdvance(dateTimePicker.Text);
        }

        private void ButtonReset_Click(object sender, EventArgs e)
        {
            todaysDate = DateTime.Now.ToShortDateString();
            labelOutputNormal.Text = logic.EncodeNormal(todaysDate);
            labelOutputAdvance.Text = logic.EncodeAdvance(todaysDate);
            dateTimePicker.ResetText();
        }
    }
}
